use std::fs;
use std::path::PathBuf;

use serde::Deserialize;

#[derive(Deserialize, Copy, Clone)]
pub struct Config {
    pub latitude: f64,
    pub longitude: f64,
}

#[derive(Deserialize)]
#[serde(rename_all = "kebab-case")]
struct CompleteConfig {
    seasonal_clock: Config,
}

pub fn get_config(filename: Option<String>) -> Option<Config> {
    let config_path = match filename {
        Some(v) => PathBuf::from(v),
        None => {
            let xdg_dirs = xdg::BaseDirectories::new().unwrap();
            xdg_dirs
                .place_config_file("seasonal-clock.toml")
                .expect("cannot create configuration directory")
        }
    };
    let data: std::io::Result<String> = fs::read_to_string(config_path);

    if let Ok(..) = data {
        let complete_config: CompleteConfig = toml::from_str(&data.unwrap()).unwrap();
        Some(complete_config.seasonal_clock)
    } else {
        eprintln!("Could not parse config file {:?}", data);
        None
    }
}
