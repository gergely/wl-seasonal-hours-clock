# Seasonal Hours Clock on Wayland

This is a Rust implementation of cinnamon’s [Seasonal Hours Clock](https://github.com/cinnamon-bun/seasonal-hours-clock), running as a native Wayland application.

## Configuration

Create a file `$XDG_CONFIG_HOME/seasonal-clock.toml` (where `$XDG_CONFIG_HOME` is `~/.config` on Linux systems) with the following content (or use/modify [example-config.toml](example-config.toml)):

```
[seasonal-clock]
latitude = 47.655
longitude = 19.286
```

(this is a location around Hungary; you may want to change these.)

If you don’t provide a configuration file, the day parts (day/night time and the golden/blue hours) won’t be visible on the clock face.

## Command line arguments

### Just print the current hour’s name

You can pass `-n` or `--now` to the app to only print the current hour’s name.

This can be used in scripts, or in i3/sway panels, for example.

### Use a different config

Pass `-c FILE` or `--config FILE` to use `FILE` as the configuration file (instead of the default).  I’m not sure if it can be useful; i only did this to show off my Rust skills ^_^

## Features

### Seasonal hours!

In cinnamon’s words:

> t would be nice if the 24 hours of UTC time each had a short memorable name. It would make it easier to plan chats with distant friends, since the hour-names would be synchronized around the world.
>
> Let's choose a theme like... seasons of the year, just to be confusing. :) Squish a year into 24 hours starting with the winter solstice at UTC 00, which we'll call The Candle Hour.

The seasonal hour names are shown on the colourful ring. Seasons are colour-coded: winter is blue, spring is green, summer is yellow, autumn is brownish/orange.

### Local time

It’s shown on the outermost ring, and as a standard watch. Currently it’s 24h only, but AM/PM version is in the works.

### UTC hours

Just so you can connect the hour names to actual numbers and to help you with inter-timezone planning. These are shown in the inner ring with a bit fainter numbers.

### Day parts

The coloured pie chart in the middle shows

- night time (very dark grey)
- pre-dawn/post-dusk time (dark blue)
- the golden hour (golden)
- daytime (bright blue)

The small red handles show the solar noon and midnight (nadir) times.

The hour hand shows the current time; the golden disc, visible only during daytime, shows the Sun’s position on the sky.

### Moon phase

The innermost circle is the current Moon phase. It tries to be as realistic as possible.

## Limitations

The day parts don’t work well (or sometimes they don’t work at all) around and beyond the arctic lines: i haven’t found a reliable way to differentiate between all-day daytime and all-day nighttime.
